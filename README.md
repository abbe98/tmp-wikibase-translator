## API

```html
<span data-wb-item="Q2">Earth</span>
<span data-wb-item="http://www.wikidata.org/entity/Q2">Earth</span>
<span data-wb-item="Q2.description">third planet from the Sun in the Solar System</span><!-- "special" way to target descriptions rather than labels  -->

<script>
    var result = WBTranslator.translate("nb,no,nn,en", updateCache=false, settings={ // the `translate` function returns the a map of the translated DOM elements and the language that was picked
        wikibases: ["https://www.wikidata.org/w/api.php"],
        setLangAttribute: false, // if true; automaticlly sets a HTML "lang" attribute on the element being translated (takes fallback language into account)
        langMap: [], // set custom mapping between Wikimedia language codes and those specified by BCP47, see https://r12a.github.io/app-subtags/
        cache: false, // if `true` the labels will be stored in local storage for offline support
        preload: [], // preload labels for given langauges (will throw an error if `cache` is `false`)
    });
</script>
```

## Examples

### Update the language if the document language changes

```html
<script>
    WBTranslator.translate(["nb", "no", "nn", "en"]);

    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.type === "attributes") {
              WBTranslator.translate(document.lang);
        }
      });
    });

    observer.observe(document, {
      attributes: true
    });
</script>
```

### Update the language for a subset of the document

### Read the resulting language for a given element

### Get the distribution data when using fallback languages  

### Automaticlly color the text background yellow for elements using a fallback language 

### Use different Wikibases for translations on the same page

## Open questions

 - Should we support properties?